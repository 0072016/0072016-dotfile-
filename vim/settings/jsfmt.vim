" Fail silently
let g:js_fmt_fail_silently = 1
" Enable auto fmt on save:
let g:js_fmt_autosave = 1
" Set the command to use for formating.
let g:js_fmt_command = "jsfmt"
" Configure jsfmt cli options.
let g:js_fmt_options = '--write'
